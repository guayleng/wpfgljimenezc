﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Libro.ViewModels
{
    public class GroupViewModel
    {
        public string Title { get; set; }
        public Group Group { get; set; }
    }
}
