﻿using Libro.Models;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Libro.Controllers
{
    class PersonController
    {
        object pwindow;
        SaveFileDialog sfdialog;
        OpenFileDialog ofdialog;
        public PersonController(PersonWindow window)
        {
            pwindow = window;
            sfdialog = new SaveFileDialog();
            ofdialog = new OpenFileDialog();
        }
        public PersonController(PersonForm form)
        {
            pwindow = form;
            sfdialog = new SaveFileDialog();
            ofdialog = new OpenFileDialog();
        }
        public void PersonEventHandler(object sender, RoutedEventArgs e)
        {
            Button B = (Button)sender;
            switch (B.Name)
            {
                case "SaveButton":
                    SaveData();
                    break;
                case "OpenButton":
                    OpenFile();
                    break;
            }
        }

        private void OpenFile()
        {
            ofdialog.Filter = "Json File (*.json)|*.json";
            if (ofdialog.ShowDialog() == true)
            {
                Person p = new Person();
                if (pwindow.GetType().Equals(typeof(PersonWindow)))
                {
                    ((PersonWindow)pwindow).SetData(p.FromJson(ofdialog.FileName));
                }
                else
                {
                    ((PersonForm)pwindow).SetData(p.FromJson(ofdialog.FileName));
                }

            }
        }

        private void SaveData()
        {
            sfdialog.Filter = "Json File (*.json)|*.json";
            if (sfdialog.ShowDialog() == true)
            {
                Person p;
                if (pwindow.GetType().Equals(typeof(PersonWindow)))
                {
                    p = ((PersonWindow)pwindow).GetData();
                }
                else
                {
                    p = ((PersonForm)pwindow).GetData();
                }

                p.ToJson(sfdialog.FileName);
            }
        }

        private void SaveCollection()
        {
            sfdialog.Filter = "Json File (*.json)|*.json";
            if (sfdialog.ShowDialog() == true)
            {
                Group g = new Group
                {
                    Name = "Grupo de demostración",
                    Members = new List<Person>
                    {
                        new Person() { FirstName = "William", LastName = "Sanchez", BirthDate = new System.DateTime(1983, 9, 1), State = "Masaya" }
                    }
                };

                Person p = new Person() { FirstName = "Douglas", LastName = "Sanchez", BirthDate = new System.DateTime(1982, 08, 17), State = "Masaya" };
                g.Members.Add(p);

                //List<Person> people = new List<Person>()
                //{
                //    new Person() { FirstName = "Erick", LastName = "Sanchez", BirthDate = new System.DateTime(1984, 9, 1), State = "Masaya" },
                //    new Person() { FirstName = "Carolina", LastName = "Sanchez", BirthDate = new System.DateTime(1985, 6, 8),State="Masaya" },
                //    new Person() { FirstName = "Ricardo", LastName = "Sanchez", BirthDate = new System.DateTime(1988, 5, 6), State="Masaya" }
                //};
                g.Members.Add(new Person() { FirstName = "Erick", LastName = "Sanchez", BirthDate = new System.DateTime(1984, 9, 1), State = "Masaya" });
                g.Members.Add(new Person() { FirstName = "Carolina", LastName = "Sanchez", BirthDate = new System.DateTime(1985, 6, 8), State = "Masaya" });
                g.Members.Add(new Person() { FirstName = "Ricardo", LastName = "Sanchez", BirthDate = new System.DateTime(1988, 5, 6), State = "Masaya" });

                g.ToJson(sfdialog.FileName);
            }
        }
    }

    public class PersonForm
    {
        internal Person GetData()
        {
            throw new NotImplementedException();
        }

        internal void SetData(Person person)
        {
            throw new NotImplementedException();
        }
    }
}
