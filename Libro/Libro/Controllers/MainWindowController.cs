﻿using Libro.Models;
using Libro.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Libro.Controllers
{
    public class MainWindowController
    {
        private MainWindow mainWindow;
        public MainWindowController(MainWindow window)
        {
            mainWindow = window;
        }
        public void MainWindowEventHandler(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.WindowState = WindowState.Maximized;
        }
        public void MainMenuEventHandler(object sender, RoutedEventArgs e)
        {
            MenuItem Option = (MenuItem)sender;
            switch (Option.Name)
            {
                case "exitAplicationMenutItem":
                    Application.Current.Shutdown();
                    break;
                case "PersonMenuItem":
                    mainWindow.DataContext = new PersonViewModel();
                    break;
                case "PersonListMenuItem":
                    mainWindow.DataContext = new GroupViewModel() { Title = "Título de la ventana", Group = GetGroup() };
                    break;


            }
        }
        private Group GetGroup()
        {
            Group g = new Group
            {
                Name = "Los 4 Fantásticos",
                Members = new List<Person>
                    {
                        new Person() { FirstName = "Calculo Diferencial y Experimental", LastName = "Erick Nasledov", BirthDate = new DateTime(2000, 1, 1), State = "Moscu" },
                        new Person() { FirstName = "Programacion POO", LastName = "Luwing Stward", BirthDate = new DateTime(1985, 1, 1), State = "Berlin" },
                        new Person() { FirstName = "Conquista de America", LastName = "Fernando Salazar", BirthDate = new DateTime(1985, 1, 1),State="Madrid" },
                        new Person() { FirstName = "Edad Media", LastName = "Richard Hathaway", BirthDate = new DateTime(1980, 1, 1), State="Londres" }
                    }
            };
            return g;
        }
    }
}
