﻿using Libro.Interface;
using Libro.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Libro.Models
{

    [Serializable]
    class Book :IToFile, IFromFile<Book>
    {
        public string ISBN { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }

        public Book FromBinary(string filepath)
        {
            throw new NotImplementedException();
        }

        public Book FromJson(string filepath)
        {
            throw new NotImplementedException();
        }

        public Book FromXml(string filepath)
        {
            throw new NotImplementedException();
        }

        public void ToBinary(string filepath)
        {
            BinarySerialization.WriteToBinaryFile(filepath, this);
        }

        public void ToJson(string filepath)
        {
            JsonSerialization.WriteToJsonFile(filepath, this);
        }

        public void ToXml(string filepath)
        {
            XmlSerialization.WriteToXmlFile(filepath, this);
        }

    }
}
