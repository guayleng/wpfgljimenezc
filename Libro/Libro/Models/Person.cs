﻿using Libro.Interface;
using Libro.Misc;
using Libro.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Libro.Models
{
    [Serializable]
    class Person : IToFile, IFromFile<Person>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public string State { get; set; }
        public int Age { get { return Util.GetAge(BirthDate); } }

        public Person FromBinary(string filepath)
        {
            return BinarySerialization.ReadFromBinaryFile<Person>(filepath);
        }

        public Person FromJson(string filepath)
        {
            return JsonSerialization.ReadFromJsonFile<Person>(filepath);
        }

        public Person FromXml(string filepath)
        {
            return XmlSerialization.ReadFromXmlFile<Person>(filepath);
        }

        public void ToBinary(string filepath)
        {
            BinarySerialization.WriteToBinaryFile(filepath, this);
        }

        public void ToJson(string filepath)
        {
            JsonSerialization.WriteToJsonFile(filepath, this);
        }

        public void ToXml(string filepath)
        {
            XmlSerialization.WriteToXmlFile(filepath, this);
        }
    
    }
}
