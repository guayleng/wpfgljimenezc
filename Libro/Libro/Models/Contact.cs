﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Libro.Models
{
    [Serializable]
    class Contact : Person
    {
        public string Email { get; set; }
        public ICollection<string> Phones { get; set; }
    }
}
